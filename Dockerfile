FROM phusion/baseimage:latest
MAINTAINER Philip G <philip@lemonaid.com>

ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

RUN \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && apt-get dist-upgrade -y -o Dpkg::Options::="--force-confold" && \
  locale-gen en_US.UTF-8 && \
  export LANG=en_US.UTF-8 && \
  apt-get install -y ca-certificates nginx && \
  chown -R www-data:www-data /var/lib/nginx && \
  apt-get clean && \
  apt-get autoremove -y && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

#RUN ln -sf /proc/$$/fd/1  /var/log/nginx/access.log && \
#  ln -sf /proc/$$/fd/1  /var/log/nginx/error.log

# This instance no longer has a need. Extended by apis and others that require nginx
# ADD runit/nginx /etc/service/nginx/run
# EXPOSE 80 443

CMD ["/sbin/my_init"]
